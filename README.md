# Módulos da Bíblia para Joomla 2.5 e 3.x

## Demo Online
http://ribafs.org/joomlademo/


Componentes e módulos contendo a Bíblia para Joomla 2.5 e 3.x em 3 idiomas: português, inglês e espanhol.

Cada pacote contém um componente e 4 módulos.

O componente tem apenas a função de instalar o banco e não aparece no administrator.

Um módulo para mostrar um versículo aleatório do Novo Testamento.

Um módulo para mostrar um versículo aleatório do Venho Testamento.

Um módulo para mostrar um pequeno form que permite buscar um versículo no Novo Testamento.

Um módulo para mostrar um pequeno form que permite buscar um versículo no Velho Testamento.

Antes de instalar descompacte o pacote e instale o componente e cada um dos módulos.

### Observação
A bíblia em espanhol tem um problema nos módulos de busca que ainda não cosnegui resolver.

## Download
https://github.com/ribafs/biblia-joomla

## Licença
GPL
